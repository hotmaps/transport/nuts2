[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687640.svg)](https://doi.org/10.5281/zenodo.4687640)

# NUTS2 (2006) level: Vehicle stock

The data in the repository are related only at the buisness as usual scenario. Further details in section 2.9 of [D2.3 WP2 Report – Open Data Set for the EU28, 2018](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf)


## Methodology

The NUTS2 data (2006) are the result of a spatial interpolation of the vehicle stock data at NUT0 level, 
using the NUTS2 data of the number of vehiclestock and the GDP at NUTS2 level as a proxy.

Furthermore, the original 5 years resolution of the [data at NUTS0 level](https://gitlab.com/hotmaps/transport/nuts0) 
has been interpolated with a yearly resolution.


## Limits of data

As the parameters at NUTS2 datasets (2006) were interpolated based on NUTS0 data, these data has to be treated cautiously. 
The data is provided at the annual basis from 1990 to 2030.


## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 

## Authors

Zubaryeva Alyona


## License


Copyright © 2016-2018: Alyona Zubaryeva

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.


